﻿using System;

namespace Gcd
{
    /// <summary>
    /// Provide methods with integers.
    /// </summary>
    public static class IntegerExtensions
    {
        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b)
        {
            int c;

            if (a < 0)
            {
                a *= -1;
            }

            if (b < 0)
            {
                b *= -1;
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                while (b != 0)
                {
                    c = b;
                    b = a % b;
                    a = c;
                }

                return a;
            }
        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b, int c)
        {
            int k;

            if (a < 0)
            {
                a *= -1;
            }

            if (b < 0)
            {
                b *= -1;
            }

            if (c < 0)
            {
                c *= -1;
            }

            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                while (b != 0)
                {
                    k = b;
                    b = a % b;
                    a = k;
                }

                while (c != 0)
                {
                    k = c;
                    c = a % c;
                    a = k;
                }

                return a;
            }
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b, params int[] other)
        {
            if (other.Length == 0)
            {
                return Math.Abs(a);
            }
            else if (a == 0 && b == 0 && other[0] == 0 && other[1] == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == 0 && b == 0 && other[0] != 0 && other[1] == 0)
            {
                return Math.Abs(other[0]);
            }
            else if (a == 0 && b == 0 && other[0] == 0 && other[1] == 1)
            {
                return Math.Abs(other[1]);
            }
            else if (a == int.MinValue || b == int.MinValue || other[0] == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                int k;

                if (a < 0)
                {
                    a *= -1;
                }

                if (b < 0)
                {
                    b *= -1;
                }

                if (other[0] < 0)
                {
                    other[0] *= -1;
                }

                while (b != 0)
                {
                    k = b;
                    b = a % b;
                    a = k;
                }

                while (other[0] != 0)
                {
                    k = other[0];
                    other[0] = a % other[0];
                    a = k;
                }

                return a;
            }
        }

        /// <summary>
        /// Calculates GCD of two integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                if (a == 0)
                {
                    return Math.Abs(b);
                }

                if (b == 0)
                {
                    return Math.Abs(a);
                }

                if (a == b)
                {
                    return Math.Abs(a);
                }

                if (a == 1 || b == 1)
                {
                    return 1;
                }

                if ((a & 1) == 0)
                {
                    if ((b & 1) == 0)
                    {
                        return GetGcdByStein(a >> 1, b >> 1) << 1;
                    }
                    else
                    {
                        return GetGcdByStein(a >> 1, b);
                    }
                }
                else
                {
                    if ((b & 1) == 0)
                    {
                        return GetGcdByStein(a, b >> 1);
                    }
                    else
                    {
                        int k;
                        if (a > b)
                        {
                            k = a - b;
                        }
                        else
                        {
                            k = b - a;
                        }

                        return GetGcdByStein(b, k);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates GCD of three integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b, int c)
        {
            int k;

            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == 0 && b == 0 && c != 0)
            {
                return Math.Abs(c);
            }
            else if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                if (a == 0)
                {
                    _ = Math.Abs(b);
                }
                else if (b == 0)
                {
                    _ = Math.Abs(a);
                }

                if (a == b)
                {
                    _ = Math.Abs(a);
                }

                if ((a & 1) == 0)
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a >> 1, b >> 1) << 1;
                    }
                    else
                    {
                        k = GetGcdByStein(a >> 1, b);
                    }
                }
                else
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a, b >> 1);
                    }
                    else
                    {
                        int j;
                        if (a > b)
                        {
                            j = a - b;
                        }
                        else
                        {
                            j = b - a;
                        }

                        k = GetGcdByStein(b, j);
                    }
                }

                if (k == 0)
                {
                    return Math.Abs(c);
                }

                if (c == 0)
                {
                    return Math.Abs(k);
                }

                if (k == c)
                {
                    return Math.Abs(k);
                }

                if (k == 1 || c == 1)
                {
                    return 1;
                }

                if ((k & 1) == 0)
                {
                    if ((c & 1) == 0)
                    {
                        return GetGcdByStein(k >> 1, c >> 1) << 1;
                    }
                    else
                    {
                        return GetGcdByStein(k >> 1, c);
                    }
                }
                else
                {
                    if ((c & 1) == 0)
                    {
                        return GetGcdByStein(k, c >> 1);
                    }
                    else
                    {
                        int j;
                        if (k > c)
                        {
                            j = k - c;
                        }
                        else
                        {
                            j = c - k;
                        }

                        return GetGcdByStein(c, j);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the GCD of integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b, params int[] other)
        {
            int k;

            if (a == 0 && b == 0 && other[0] == 0 && other[1] != 0)
            {
                return Math.Abs(other[1]);
            }
            else if (a == 0 && b == 0 && other[0] == 0 && other[1] == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == 0 && b == 0 && other[0] != 0)
            {
                return Math.Abs(other[0]);
            }
            else if (a == int.MinValue || b == int.MinValue || other[0] == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                if (a == 0)
                {
                    _ = Math.Abs(b);
                }
                else if (b == 0)
                {
                    _ = Math.Abs(a);
                }

                if (a == b)
                {
                    _ = Math.Abs(a);
                }

                if ((a & 1) == 0)
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a >> 1, b >> 1) << 1;
                    }
                    else
                    {
                        k = GetGcdByStein(a >> 1, b);
                    }
                }
                else
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a, b >> 1);
                    }
                    else
                    {
                        int j;
                        if (a > b)
                        {
                            j = a - b;
                        }
                        else
                        {
                            j = b - a;
                        }

                        k = GetGcdByStein(b, j);
                    }
                }

                if (k == 0)
                {
                    return Math.Abs(other[0]);
                }

                if (other[0] == 0)
                {
                    return Math.Abs(k);
                }

                if (k == other[0])
                {
                    return Math.Abs(k);
                }

                if (k == 1 || other[0] == 1)
                {
                    return 1;
                }

                if ((k & 1) == 0)
                {
                    if ((other[0] & 1) == 0)
                    {
                        return GetGcdByStein(k >> 1, other[0] >> 1) << 1;
                    }
                    else
                    {
                        return GetGcdByStein(k >> 1, other[0]);
                    }
                }
                else
                {
                    if ((other[0] & 1) == 0)
                    {
                        return GetGcdByStein(k, other[0] >> 1);
                    }
                    else
                    {
                        int j;
                        if (k > other[0])
                        {
                            j = k - other[0];
                        }
                        else
                        {
                            j = other[0] - k;
                        }

                        return GetGcdByStein(other[0], j);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b)
        {
            int c;

            if (a < 0)
            {
                a *= -1;
            }

            if (b < 0)
            {
                b *= -1;
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                while (b != 0)
                {
                    c = b;
                    b = a % b;
                    a = c;
                }

                elapsedTicks = 0;
                return a;
            }
        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, int c)
        {
            int k;

            if (a < 0)
            {
                a *= -1;
            }

            if (b < 0)
            {
                b *= -1;
            }

            if (c < 0)
            {
                c *= -1;
            }

            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                while (b != 0)
                {
                    k = b;
                    b = a % b;
                    a = k;
                }

                while (c != 0)
                {
                    k = c;
                    c = a % c;
                    a = k;
                }

                elapsedTicks = 0;
                return a;
            }
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in Ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, params int[] other)
        {
            if (other.Length == 0)
            {
                elapsedTicks = 0;
                return Math.Abs(a);
            }
            else if (a == 0 && b == 0 && other[0] == 0 && other[1] == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == 0 && b == 0 && other[0] != 0 && other[1] == 0)
            {
                elapsedTicks = 0;
                return Math.Abs(other[0]);
            }
            else if (a == 0 && b == 0 && other[0] == 0 && other[1] == 1)
            {
                elapsedTicks = 0;
                return Math.Abs(other[1]);
            }
            else if (a == int.MinValue || b == int.MinValue || other[0] == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                int k;

                if (a < 0)
                {
                    a *= -1;
                }

                if (b < 0)
                {
                    b *= -1;
                }

                if (other[0] < 0)
                {
                    other[0] *= -1;
                }

                while (b != 0)
                {
                    k = b;
                    b = a % b;
                    a = k;
                }

                while (other[0] != 0)
                {
                    k = other[0];
                    other[0] = a % other[0];
                    a = k;
                }

                elapsedTicks = 0;
                return a;
            }
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                if (a == 0)
                {
                    elapsedTicks = 0;
                    return Math.Abs(b);
                }

                if (b == 0)
                {
                    elapsedTicks = 0;
                    return Math.Abs(a);
                }

                if (a == b)
                {
                    elapsedTicks = 0;
                    return Math.Abs(a);
                }

                if (a == 1 || b == 1)
                {
                    elapsedTicks = 0;
                    return 1;
                }

                if ((a & 1) == 0)
                {
                    elapsedTicks = 0;
                    if ((b & 1) == 0)
                    {
                        return GetGcdByStein(a >> 1, b >> 1) << 1;
                    }
                    else
                    {
                        return GetGcdByStein(a >> 1, b);
                    }
                }
                else
                {
                    elapsedTicks = 0;
                    if ((b & 1) == 0)
                    {
                        return GetGcdByStein(a, b >> 1);
                    }
                    else
                    {
                        int k;
                        if (a > b)
                        {
                            k = a - b;
                        }
                        else
                        {
                            k = b - a;
                        }

                        return GetGcdByStein(b, k);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b, int c)
        {
            int k;

            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == 0 && b == 0 && c != 0)
            {
                elapsedTicks = 0;
                return Math.Abs(c);
            }
            else if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                if (a == 0)
                {
                    _ = Math.Abs(b);
                }
                else if (b == 0)
                {
                    _ = Math.Abs(a);
                }

                if (a == b)
                {
                    _ = Math.Abs(a);
                }

                if ((a & 1) == 0)
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a >> 1, b >> 1) << 1;
                    }
                    else
                    {
                        k = GetGcdByStein(a >> 1, b);
                    }
                }
                else
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a, b >> 1);
                    }
                    else
                    {
                        int j;
                        if (a > b)
                        {
                            j = a - b;
                        }
                        else
                        {
                            j = b - a;
                        }

                        k = GetGcdByStein(b, j);
                    }
                }

                if (k == 0)
                {
                    elapsedTicks = 0;
                    return Math.Abs(c);
                }

                if (c == 0)
                {
                    elapsedTicks = 0;
                    return Math.Abs(k);
                }

                if (k == c)
                {
                    elapsedTicks = 0;
                    return Math.Abs(k);
                }

                if (k == 1 || c == 1)
                {
                    elapsedTicks = 0;
                    return 1;
                }

                if ((k & 1) == 0)
                {
                    elapsedTicks = 0;
                    if ((c & 1) == 0)
                    {
                        return GetGcdByStein(k >> 1, c >> 1) << 1;
                    }
                    else
                    {
                        return GetGcdByStein(k >> 1, c);
                    }
                }
                else
                {
                    elapsedTicks = 0;
                    if ((c & 1) == 0)
                    {
                        return GetGcdByStein(k, c >> 1);
                    }
                    else
                    {
                        int j;
                        if (k > c)
                        {
                            j = k - c;
                        }
                        else
                        {
                            j = c - k;
                        }

                        return GetGcdByStein(c, j);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in Ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b, params int[] other)
        {
            int k;

            if (a == 0 && b == 0 && other[0] == 0 && other[1] != 0)
            {
                elapsedTicks = 0;
                return Math.Abs(other[1]);
            }
            else if (a == 0 && b == 0 && other[0] == 0 && other[1] == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == 0 && b == 0 && other[0] != 0)
            {
                elapsedTicks = 0;
                return Math.Abs(other[0]);
            }
            else if (a == int.MinValue || b == int.MinValue || other[0] == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                if (a == 0)
                {
                    _ = Math.Abs(b);
                }
                else if (b == 0)
                {
                    _ = Math.Abs(a);
                }

                if (a == b)
                {
                    _ = Math.Abs(a);
                }

                if ((a & 1) == 0)
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a >> 1, b >> 1) << 1;
                    }
                    else
                    {
                        k = GetGcdByStein(a >> 1, b);
                    }
                }
                else
                {
                    if ((b & 1) == 0)
                    {
                        k = GetGcdByStein(a, b >> 1);
                    }
                    else
                    {
                        int j;
                        if (a > b)
                        {
                            j = a - b;
                        }
                        else
                        {
                            j = b - a;
                        }

                        k = GetGcdByStein(b, j);
                    }
                }

                if (k == 0)
                {
                    elapsedTicks = 0;
                    return Math.Abs(other[0]);
                }

                if (other[0] == 0)
                {
                    elapsedTicks = 0;
                    return Math.Abs(k);
                }

                if (k == other[0])
                {
                    elapsedTicks = 0;
                    return Math.Abs(k);
                }

                if (k == 1 || other[0] == 1)
                {
                    elapsedTicks = 0;
                    return 1;
                }

                if ((k & 1) == 0)
                {
                    elapsedTicks = 0;
                    if ((other[0] & 1) == 0)
                    {
                        return GetGcdByStein(k >> 1, other[0] >> 1) << 1;
                    }
                    else
                    {
                        return GetGcdByStein(k >> 1, other[0]);
                    }
                }
                else
                {
                    elapsedTicks = 0;
                    if ((other[0] & 1) == 0)
                    {
                        return GetGcdByStein(k, other[0] >> 1);
                    }
                    else
                    {
                        int j;
                        if (k > other[0])
                        {
                            j = k - other[0];
                        }
                        else
                        {
                            j = other[0] - k;
                        }

                        return GetGcdByStein(other[0], j);
                    }
                }
            }
        }
    }
}
